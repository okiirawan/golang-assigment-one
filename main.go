package main

import (
	"fmt"
	"os"
)

type Biodata struct {
	Id        int
	Nama      string
	Alamat    string
	Pekerjaan string
	Alasan    string
}

func (b *Biodata) showingBiodata() {
	fmt.Printf("ID : %d\n", b.Id)
	fmt.Printf("nama : %s\n", b.Nama)
	fmt.Printf("alamat : %s\n", b.Alamat)
	fmt.Printf("pekerjaan : %s\n", b.Pekerjaan)
	fmt.Printf("alasan : %s\n", b.Alasan)
}

func SearchData(args, data []string, biodata *Biodata) {
	var inputedValue = ""

	if len(args) > 1 {
		inputedValue = args[1]
	}

	if inputedValue == "" {
		fmt.Printf("No arguments inputed \n")
		return
	}

	for index, value := range data {
		if inputedValue == value {
			biodata.Id = index
			biodata.Nama = value
			biodata.Alamat = "Bekasi"
			biodata.Pekerjaan = "Product Manager"
			biodata.Alasan = "Expert Programming"
			break
		}
	}
}

func main() {
	var (
		data    = []string{"Oki", "Irawan", "Yorda", "Ghita"}
		biodata = Biodata{}
	)

	args := os.Args
	SearchData(args, data, &biodata)

	if biodata == (Biodata{}) {
		fmt.Printf("No data found")
	} else {
		biodata.showingBiodata()
	}

}
